﻿using Microsoft.EntityFrameworkCore;
using NorthwindCustomer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NorthwindCustomer
{
    class Northwind : DbContext
    {
        // Zbiorek 1: Categories
        public DbSet<Customer> Customers { get; set; }
        // Zbiorek 2: Products

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, "northwind.sqlite");

            optionsBuilder.UseSqlite($"Filename={path}");

        }
    }
}
