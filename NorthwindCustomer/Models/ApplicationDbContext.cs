﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindCustomer.Models
{
    public class ApplicationDbContext : DbContext
    {

        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var path = @"C:\Users\Szymon\source\repos\NorthwindCustomer\NorthwindCustomer\db\northwind.sqlite";
            optionsBuilder.UseSqlite($"Filename={path}");
            base.OnConfiguring(optionsBuilder);


        }



    }
}
