﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindCustomer.Models
{
    [Table("Customer")]
    public class Customer
    {
       
        public string Id { get; set; }
        public string ContactName { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
    }
}
